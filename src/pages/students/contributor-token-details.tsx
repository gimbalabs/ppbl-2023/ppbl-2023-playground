import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text, Center } from "@chakra-ui/react";
import React from "react";
import { QueryContributorTokenDetails } from "../../components/QueryContributorTokenDetails";

export default function ContributorTokenDetails() {
  
  return (
    <>
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Divider w="90%" mx="auto" pb="5" />

      <Box w={["100%", "90%"]} mx="auto" my="10">

        <Center><Heading>Contributor Token Details</Heading></Center>

        <Divider w="100%" mx="auto" pb="7" />

        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">

          <Heading size="md" pb="3">
            For a given PPBL2023 token alias, this page provides information like its current lucky number and the list of modules completed.
            It is similar to the information provided at
            <CLink color="teal.400" href="https://plutuspbl.io/contributors"> PPBL Contributors.</CLink>
          </Heading>

          <br/>
          <QueryContributorTokenDetails/>
          <br/>

          <Heading size="md" pb="3">Some Explanations:</Heading>
          <Text pb="3">
            I am using a GraphQL query to fetch this information. The query takes the AssetId {'(policyId + tokenName)'} of the 
            Contributor Reference Token and finds the UTxO in which this token is present. This UTxO's datum contains the real time
            value of the token's lucky number and the list of completed modules. <b>UTxO == Present</b>
            <br/>
            When you initiate a commitment transaction, which locks your contributor token in an escrow contract, nothing happens
            to this reference contributor token UTxO which is peacefully sitting at another contract address. It is vital to know that
            the ref token never leaves this contract.
            <br/>           
            It's only when you successfully complete the committment that a PPBL instructor comes along and initiates an interesting 
            transaction. This transaction spends the Ref token UTxO and the Contributor token
            {' (which is sitting locked at the escrow contract)'}. What it does next is the fun part. It first returns the Contributor
            token back to the Contributor's address. Along with that it returns the Ref token back to the contract where its always sitting
            with an updated datum which now has the newly added module for which the committment was completed.
            <br/>
            All of this {' (and a bit more, which I skipped out on)'} happened in a single transaction. Phew!
          </Text>
        </Box>
      </Box>
    </>
  );
}
