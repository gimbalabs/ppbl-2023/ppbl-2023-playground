import { Box, Text, Input, Button, Grid, GridItem } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useFormik } from "formik";
import { KoiosProvider } from "@meshsdk/core";


export default function StudentTemplate() {
  const koiosProvider = new KoiosProvider('preprod');
  const [stakeAddress, setStakeAddress] = useState("");
  const [myAssets, setMyAssets] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // Show a loading animation/message while loading
    setLoading(true);
     
    const fetchStakeData = async () => { 
      console.log("fetching")
      const _data = await koiosProvider.fetchAccountInfo(stakeAddress)
      console.log(_data)
      if (_data != undefined) {

        setMyAssets([stakeAddress, _data.balance])
        //console.log(stakeAddress)
        //console.log(_data.balance)
        setLoading(false);
      }
    }

    if (stakeAddress != "") {
      fetchStakeData()
    }

  }, [stakeAddress]);

  const formik = useFormik({
    initialValues: {
      inputAddress: '',
    },
    onSubmit: (values) => {
      setStakeAddress(values.inputAddress);
      //console.log(values.inputAddress)
    },
  });
   
  return (
    <>
        <Box bg="theme.light" color="theme.dark" p="3">
        <form onSubmit={formik.handleSubmit}>
        <Input 
          id="inputAddress" 
          name="inputAddress"
          bg={"white"}
          onChange={formik.handleChange}
          value={formik.values.inputAddress}
          border='1px' placeholder='Enter your stake address' />
        
        <Button type='submit' colorScheme='green' mb= '20px' mt='20px' p='30px'>Submit</Button>
        </form>

        <Grid templateColumns='repeat(2, 1fr)' gap={1}>
             <GridItem key={-2} w='100%' bg='blue.500'><Text p='2' align={"right"}>Stake Address</Text></GridItem> 
             <GridItem key={-1} w='100%' bg='blue.500'><Text p='2' align={"right"}>Balance (ADA)</Text></GridItem> 
             
             {loading ? ("Loading") : (myAssets.map((item, index) => <GridItem key={index} w='100%' bg='blue.500'><Text p='2' align={"right"}>{item}</Text></GridItem> ))}
        </Grid>
        </Box>
    </>
  );
}
