import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text } from "@chakra-ui/react";
import { CardanoWallet, useAssets, useWallet } from "@meshsdk/react";
import { ContributorComponent } from "@/src/components/ContributorComponent";
import { QueryComponent } from "@/src/components/QueryComponent";

export default function StudentTemplate() {
  
  const { wallet, connected, name, connecting, connect, disconnect, error } = useWallet();
  const assets = useAssets();

  return (
    <>
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Divider w="70%" mx="auto" pb="10" />

      <Box w={["100%", "70%"]} mx="auto" my="10">

        <Heading>Check all my Tokens</Heading>

        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">

          <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
            <Text mb="4">Please connect your wallet to see what tokens your wallet holds</Text>
            <CardanoWallet />
          </Box>

          <Box>
            {assets?.map((item, i) => (
              <Box mt="3" key={i}>
                {item.quantity} {item.assetName}
              </Box>
            )
            )}
          </Box>

        </Box>

      </Box>
    </>
  );
}
