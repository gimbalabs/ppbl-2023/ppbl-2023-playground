import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text } from "@chakra-ui/react";
import { CardanoWallet } from "@meshsdk/react";
import { ContributorComponent } from "@/src/components/ContributorComponent";
import { QueryComponent } from "@/src/components/QueryComponent";

export default function StudentTemplate() {
  return (
    <>
      <Head>
        <title>PPBL 2023 Playground: Newman5</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Divider w="70%" mx="auto" pb="10" />
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading>What does Newman want to build?</Heading>
        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">
          <Heading size="md" pb="3">
            Newman's ideas:
          </Heading>
<li> a kick-ass - but simple and small, like kicking the ass of an ant.  It's a tiny ass but... thinking about it the ant ass is RELATIVE to the body a huge ass.  Well my learning game will be like that, it's tiny and big depending on one's perspective.  To an ant, it's gonna be huge~!</li>
<li>A cool glossary of terms for new Plutus PBL learners and I'm workin on it <a href="https://hackmd.io/Dz5X9niuQ-S0i4cydNou1A?both">HERE</a></li>
<li>A way to drag and drop items in a googlesheet to order a list graphically - for example a list of proposals listed for funding.  And, could we get ranked choice voting ...  </li>
        </Box>
        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">
          <Heading size="md" pb="3">
            Checking your PPBL 2023 Contributor Token:
          </Heading>
          <div style={{ position: 'absolute', top: 0, right: 0 }}>
            <CardanoWallet />
          </div>
          <ContributorComponent />
          <QueryComponent />
        </Box>
      </Box>
    </>
  );
}
