// Import Statements:
import {
	Box,
	Heading,
	FormControl,
	FormLabel,
	Input,
	Button,
	Text,
  Select
} from '@chakra-ui/react';
import { useFormik } from 'formik';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { KoiosProvider, UTxO } from '@meshsdk/core';

enum Network {
  Mainnet = 'api',
  Preprod = 'preprod',
  Preview = 'preview',
}

// The <KoiosComponentExample /> that can be imported to /ppbl-2023-front-end-starter/src/pages/index.tsx
export default function StudentTemplate() {
	// useState hook to store address from user input
	const [queryAddress, setQueryAddress] = useState<string | undefined>(
		undefined
	);
  // useState hook to track address validation
	const [isValidAddress, setIsValidAddress] = useState<boolean>(true); 
  // useState hook to track query environment
  const [network, setNetwork] = useState<Network>(
    Network.Preprod
  );

	const koiosProvider = new KoiosProvider(network);
	const [koiosResultUTxOs, setKoiosResultUTxOs] = useState<UTxO[] | undefined>(
		undefined
	);

	// useFormik hook from formik library
	// Makes it easier to handle forms in React
	const formik = useFormik({
		initialValues: {
			cardanoAddress: '',
		},
		onSubmit: (values) => {
			setQueryAddress(values.cardanoAddress);
		},
	});

  // When a button is clicked or Enter key is pressed, set the queryAddress
  const handleClick = () => {
    validateAndSetQueryAddress(formik.values.cardanoAddress);
  };

  const handleEnterKeyPress = (event: { key: string }) => {
    if (event.key === 'Enter') {
      validateAndSetQueryAddress(formik.values.cardanoAddress);
    }
  };

  const validateAndSetQueryAddress = (address: string) => {
    const isValidAddr = validateAddress(address);
    setIsValidAddress(isValidAddr);
    if (isValidAddr) {
      setQueryAddress(address);
    }
  };

  // Function to validate the Cardano address
  const validateAddress = (address: string): boolean => {
    let simpleAddressRegex;
    if (network === Network.Mainnet) {
      simpleAddressRegex = /^addr1[0-9a-zA-Z]{53}$|^addr1[0-9a-zA-Z]{98}$/;
    } else {
      simpleAddressRegex = /^addr_test1[0-9a-zA-Z]{53}$|^addr_test1[0-9a-zA-Z]{98}$/;
    }
    return simpleAddressRegex.test(address);
  };

	// When queryAddress is set, use koiosProvider.fetchAddressUTxOs to get get UTxOs at address
	useEffect(() => {
		const fetchAddressUTxOs = async () => {
			if (queryAddress) {
				const result = await koiosProvider.fetchAddressUTxOs(queryAddress);
				setKoiosResultUTxOs(result);
			}
		};
		if (queryAddress) {
			fetchAddressUTxOs();
		}
	}, [queryAddress]);

	// Rendered on the page:
	return (
		<Box bg='theme.light' color='theme.dark' p='3' mt='5'>
			<Heading size='md' py='3'>
        Query all UTxOs for Cardano address
			</Heading>
			<Text py='3'>
        Add a simple address checking. Press enter or click button to get the result.
			</Text>
			<FormControl bg='theme.dark' color='theme.light' p='5'>
        <Text py='3'>Network:</Text>
        <Select
          mb="3"
          id="network"
          name="network"
          onChange={(event) =>
            setNetwork(event.target.value as Network)
          }
          value={network}
        >
          <option value={Network.Mainnet}>Mainnet</option>
          <option value={Network.Preprod}>Preprod</option>
          <option value={Network.Preview}>Preview</option>
        </Select>

        <Text py='3'>Enter an address:</Text>
				<Input
					mb='3'
					id='cardanoAddress'
					name='cardanoAddress'
					onChange={formik.handleChange}
          onKeyDown={handleEnterKeyPress} // Listen for Enter key press
					value={formik.values.cardanoAddress}
					placeholder='Cardano Address'
				/>

				<Button onClick={handleClick} size='sm'>
					Check Address
				</Button>
			</FormControl>
      {!isValidAddress && (
        <Box bg="red.400" color="theme.dark" mt="5" p="3" fontSize="sm">
          Invalid address. Please check!
        </Box>
      )}
			{koiosResultUTxOs && isValidAddress && (
				<Box fontSize='xs'>
					<pre>{JSON.stringify(koiosResultUTxOs, null, 2)}</pre>
				</Box>
			)}
		</Box>
	);
};
