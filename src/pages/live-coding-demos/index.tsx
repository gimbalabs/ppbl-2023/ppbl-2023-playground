import Head from "next/head";
import {
  Box,
  Divider,
  Link as CLink,
  Heading,
  Text,
  UnorderedList,
  ListItem,
  FormControl,
  FormLabel,
  Input,
  FormHelperText,
  Button,
} from "@chakra-ui/react";
import { ContributorComponent } from "@/src/components/ContributorComponent";
import { QueryComponent } from "@/src/components/QueryComponent";
import { CardanoWallet } from "@meshsdk/react";
import Link from "next/link";
import { Formik, Field, Form, useField, useFormikContext } from "formik";
import { useEffect } from "react";

// Live Coding Playground Session 2023-05-04
// https://formik.org/docs/examples/dependent-fields

export default function Home() {
  return (
    <Formik
      initialValues={{ age: 0, maxHeartRate: 0 }}
      validate={(values) => {
        const errors = {};
        if (values.maxHeartRate > 220 - values.age) {
          errors.maxHeartRate = "Max Heart Rate should be below " + (220 - values.age);
        }
        return errors;
      }}
      onSubmit={(values) => console.log(values)}
    >
      {({ values, setFieldValue }) => (
        <Form>
          <Field
            name="age"
            placeholder="Age"
            type="number"
            onChange={(e) => {
              setFieldValue("age", e.target.value);
              setFieldValue("maxHeartRate", e.target.value / 2);
            }}
          />
          <Field
            name="maxHeartRate"
            placeholder="Max Heart Rate"
            type="number"
            onChange={(e) => {
              setFieldValue("maxHeartRate", e.target.value);
              setFieldValue("age", e.target.value * 2);
            }}
          />
          <button type="submit">Submit</button>
        </Form>
      )}
    </Formik>
  );
}

// ----------------------------------------------------------------
// First option explored on 2023-05-04

// type Props = {
//   targetFieldName: string;
// };

// const MyField: React.FC<Props> = (props) => {
//   const {
//     values: { textA, textB },
//     touched,
//     setFieldValue,
//   } = useFormikContext();
//   const [field, meta] = useField(props.targetFieldName);

//   console.log("Initial touched A", touched.textA)
//   console.log("Initial touched B", touched.textB)

//   useEffect(() => {
//     // set the value of textB, based on textA
//     if (textA.trim() !== "" && touched.textA) {
//       setFieldTouched(textA, false);
//       setFieldValue(props.targetFieldName, textA);
//     } else if (textB.trim() !== "" && touched.textB) {
//       setFieldValue(props.targetFieldName, textB);
//     }
//   }, [textA, touched.textA, textB, touched.textB, setFieldValue, props.targetFieldName]);

//   return (
//     <>
//       <input {...props} {...field} />
//       {!!meta.touched && !!meta.error && <div>{meta.error}</div>}
//     </>
//   );
// };

// export default function Home() {
//   const initialValues = { textA: "", textB: "" };

//   return (
//     <>
//       <Head>
//         <title>PPBL 2023 Playground</title>
//         <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
//         <meta name="viewport" content="width=device-width, initial-scale=1" />
//         <link rel="icon" href="/favicon.ico" />
//       </Head>
//       <Box w="90%" mx="auto" my="5">
//         <Heading>Live Coding Forms Demo</Heading>
//         <Box my="5" p="5" bg="orange.100" color="green.900">
//           Form Experiments
//         </Box>

//         <Formik initialValues={initialValues} onSubmit={async (values) => alert(JSON.stringify(values, null, 2))}>
//           <Form>
//             <label>
//               textA
//               <MyField targetFieldName="textA" />
//             </label>
//             <label>
//               textB
//               <MyField targetFieldName="textB" />
//             </label>
//             <Button colorScheme="green" type="submit">
//               Submit Form
//             </Button>
//           </Form>
//         </Formik>
//         {/* <FormControl color="theme.dark">
//           <FormLabel color="theme.light">Value1:</FormLabel>
//           <Input
//             mb="3"
//             bg="white"
//             id="value1"
//             name="value1"
//             onChange={formik.handleChange}
//             value={formik.values.value1}
//             placeholder="Value 1"
//           />
//           <FormLabel color="theme.light">Value2:</FormLabel>
//           <Input
//             mb="3"
//             bg="white"
//             id="value2"
//             name="value2"
//             onChange={formik.handleChange}
//             value={formik.values.value2}
//             placeholder="Value 2"
//             </FormControl>
//           /> */}
//         <Box p="5">
//           {/* <Text fontSize="4xl">{formik.values.value1}</Text>
//           <Text fontSize="4xl">{formik.values.value2}</Text> */}
//         </Box>
//       </Box>
//     </>
//   );
// }
