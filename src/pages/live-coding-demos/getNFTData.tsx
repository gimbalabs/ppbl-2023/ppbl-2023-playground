import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";

// Further reading: https://gitlab.com/gimbalabs/ppbl-2023/ppbl-front-end-template-2023/-/blob/main/src/components/lms/Mastery/CourseMasteryOverview.tsx?ref_type=heads




export default function GetNFTData() {
  const [ourNFTData, setNFTData] = useState<any | undefined>(undefined)


  useEffect(() => {
    setNFTData("Hello there")
  }, [])

  return (
    <>
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w="60%" mx="auto" py="24" bg="white" color="black">
        <Heading>NFT Data Example</Heading>
        <Box>
          This is our nft data: {ourNFTData}
        </Box>
      </Box>
    </>
  );
}
